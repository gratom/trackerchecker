﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Global.Managers.Datas;
using Global.Managers.UserInterface;
using Tools;
using UnityEngine;
using UnityEngine.Networking;

namespace Global.Managers.Main
{
    public class MainManager : BaseManager
    {
        public override Type ManagerType => typeof(MainManager);

        public List<DayWork> days;

        private Root jsonClass;

        protected override bool OnInit()
        {
            return true;
        }

        public void EntryPoint()
        {
            if (string.IsNullOrEmpty(Services.GetManager<DataManager>().DynamicData.Settings.PAT))
            {
                Services.GetManager<UIManager>().ShowWindow<MainMenuWindow>();
            }
            else
            {
                StartLogin(() => { Services.GetManager<UIManager>().ShowWindow<MainMenuWindow>().RefreshData(); });
            }
        }

        public void StartLogin(Action onFinish)
        {
            TryForm(onFinish);
        }

        public async System.Threading.Tasks.Task TryForm(Action onFinish)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                using (HttpRequestMessage request = new HttpRequestMessage(new HttpMethod("GET"), "https://api.harvestapp.com/v2/time_entries"))
                {
                    Debug.Log("Try to this pat : " + Services.GetManager<DataManager>().DynamicData.Settings.PAT);
                    request.Headers.TryAddWithoutValidation("Harvest-Account-ID", "1153152");
                    request.Headers.TryAddWithoutValidation("Authorization", "Bearer " + Services.GetManager<DataManager>().DynamicData.Settings.PAT);
                    request.Headers.TryAddWithoutValidation("User-Agent", "Harvest API Example");

                    HttpResponseMessage response = await httpClient.SendAsync(request);
                    string responseBody = await response.Content.ReadAsStringAsync();
                    SaverLoaderModule.SaveMyDataToFile("jsonfile.txt", responseBody);
                    jsonClass = JsonUtility.FromJson<Root>(responseBody);

                    days = new List<DayWork>();

                    var lists = jsonClass.time_entries.GroupBy(x => x.spent_date);

                    foreach (IGrouping<string, TimeEntry> timeEntries in lists)
                    {
                        days.Add(new DayWork() { stringDate = timeEntries.Key, works = new List<TaskWork>() });
                        foreach (TimeEntry timeEntry in timeEntries)
                        {
                            days[days.Count - 1]
                                .works.Add(new TaskWork()
                                {
                                    time = timeEntry.hours,
                                    project = timeEntry.project.name,
                                    name = timeEntry.task.name,
                                    creatingDay = timeEntry.created_at.Substring(0, 10),
                                    creatingTime = timeEntry.created_at.Substring(11, 8),
                                    changingDay = timeEntry.updated_at.Substring(0, 10),
                                    changingTime = timeEntry.updated_at.Substring(11, 8),
                                    isStrange = timeEntry.updated_at.Substring(0, 10) != timeEntry.created_at.Substring(0, 10)
                                });
                        }
                    }

                    Services.GetManager<DataManager>().DynamicData.Settings.updates.Add(new State() { days = days, TimeSpan = DateTime.Now.ToShortDateString() });
                    Debug.Log("Ready");
                    onFinish?.Invoke();
                }
            }
        }
    }

    [Serializable]
    public class DayWork
    {
        public string stringDate;
        public List<TaskWork> works;
        public double totalTime => works.Sum(x => x.time);
    }

    [Serializable]
    public class TaskWork
    {
        public string name;
        public double time;
        public string project;
        public string creatingDay;
        public string creatingTime;
        public string changingDay;
        public string changingTime;
        public bool isStrange;
    }

    [Serializable]
    public class User
    {
        public int id;
        public string name;
    }

    [Serializable]
    public class Client
    {
        public int id;
        public string name;
        public string currency;
    }

    [Serializable]
    public class Project
    {
        public int id;
        public string name;
        public string code;
    }

    [Serializable]
    public class Task
    {
        public int id;
        public string name;
    }

    [Serializable]
    public class UserAssignment
    {
        public int id;
        public bool is_project_manager;
        public bool is_active;
        public bool use_default_rates;
        public object budget;
        public DateTime created_at;
        public DateTime updated_at;
        public object hourly_rate;
    }

    [Serializable]
    public class TaskAssignment
    {
        public int id;
        public bool billable;
        public bool is_active;
        public DateTime created_at;
        public DateTime updated_at;
        public object hourly_rate;
        public object budget;
    }

    [Serializable]
    public class TimeEntry
    {
        public int id;
        public string spent_date;
        public double hours;
        public double hours_without_timer;
        public double rounded_hours;
        public string notes;
        public bool is_locked;
        public object locked_reason;
        public bool is_closed;
        public bool is_billed;
        public DateTime? timer_started_at;
        public string started_time;
        public object ended_time;
        public bool is_running;
        public bool billable;
        public bool budgeted;
        public object billable_rate;
        public object cost_rate;
        public string created_at;
        public string updated_at;
        public User user;
        public Client client;
        public Project project;
        public Task task;
        public UserAssignment user_assignment;
        public TaskAssignment task_assignment;
        public object invoice;
        public object external_reference;
    }

    [Serializable]
    public class Links
    {
        public string first;
        public string next;
        public object previous;
        public string last;
    }

    [Serializable]
    public class Root
    {
        public List<TimeEntry> time_entries;
        public int per_page;
        public int total_pages;
        public int total_entries;
        public int next_page;
        public object previous_page;
        public int page;
        public Links links;
    }
}
