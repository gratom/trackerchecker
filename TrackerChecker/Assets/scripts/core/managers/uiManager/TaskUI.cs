﻿using Global.Managers.Main;
using UnityEngine;
using UnityEngine.UI;

public class TaskUI : MonoBehaviour
{
    [SerializeField] public Text taskNameAndProject;
    [SerializeField] public Text time;
    [SerializeField] public Text creating;
    [SerializeField] public Text changing;
    [SerializeField] public Image isStrange;

    public void LoadTask(TaskWork task)
    {
        taskNameAndProject.text = task.project + ":" + task.name;
        time.text = task.time.ToString("0.00");
        creating.text = "Created at :" + task.creatingDay + " - " + task.creatingTime;
        changing.text = "Changed at :" + task.changingDay + " - " + task.changingTime;
        isStrange.color = task.isStrange ? Color.red : Color.green;
    }
}
