﻿using System;
using Global.Managers.Main;
using Tools;
using UnityEngine;
using UnityEngine.UI;

public class DayUI : MonoBehaviour
{
    [SerializeField] private TaskUI taskPrefab;
    [SerializeField] private Text dateText;
    [SerializeField] private Text totalDayTime;
    [SerializeField] private RectTransform parentTaskTransform;

    public void LoadDay(DayWork day)
    {
        parentTaskTransform.Clear();
        dateText.text = day.stringDate;
        totalDayTime.text = day.totalTime.ToString("0.00");
        foreach (TaskWork task in day.works)
        {
            TaskUI taskUI = Instantiate(taskPrefab, parentTaskTransform);
            taskUI.LoadTask(task);
        }
    }
}
