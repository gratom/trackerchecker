﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Global;
using Global.Components.UserInterface;
using Global.Managers.Datas;
using Global.Managers.Main;
using Tools;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuWindow : BaseWindow
{
    [SerializeField] private InputField patInput;
    [SerializeField] private DayUI dayUIPrefab;
    [SerializeField] private Button loginButton;

    [SerializeField] private RectTransform parentGameobject;

    protected override void OnHide()
    {
    }

    protected override void OnShow()
    {
        patInput.text = Services.GetManager<DataManager>().DynamicData.Settings.PAT;
    }

    public void OnLoginClick()
    {
        loginButton.interactable = false;
        Services.GetManager<DataManager>().DynamicData.Settings.PAT = patInput.text;
        Services.GetManager<MainManager>().StartLogin(() =>
        {
            RefreshData();
            loginButton.interactable = true;
        });
    }

    public void RefreshData()
    {
        parentGameobject.Clear();
        State last = Services.GetManager<DataManager>().DynamicData.Settings.updates.Last();
        foreach (DayWork day in last.days)
        {
            DayUI dayUI = Instantiate(dayUIPrefab, parentGameobject);
            dayUI.LoadDay(day);
        }
    }

    public void ExitClick()
    {
        Application.Quit();
    }
}
