﻿using System;
using System.Collections.Generic;
using Global.Managers.Main;
using UnityEngine;

namespace Global.Managers.Datas
{
    /// <summary>
    /// Class for saving application states.
    /// The state is, for example, the name of the character,
    /// the amount of money the player has,
    /// the saving of the game, the settings,
    /// all the data that can change during the game and that need to be saved.
    /// </summary>
    [Serializable]
    public class DynamicData
    {
#pragma warning disable

        [SerializeField, Storage(Storage.StoragePathType.applicationFolder, "Settings")]
        private UserData settings;

#pragma warning restore

        public UserData Settings => settings;
    }

    [Serializable]
    public class UserData
    {
        public string PAT;
        public List<State> updates = new List<State>();
    }

    [Serializable]
    public class State
    {
        public string TimeSpan;
        public List<DayWork> days;
    }
}
